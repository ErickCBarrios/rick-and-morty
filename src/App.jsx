import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

import { Link, Route, Routes } from 'react-router-dom';

import { Footer } from './components/Footer/Footer';
import { Header } from './components/Narbar/Header';
import { Home } from './pages/Home';
import { Personajes } from './pages/Personajes';
import { useState } from 'react';
import './assets/css/App.css';
function App() {
  return (
    <div className='App.css'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Personaje/:personajeId' element={<Personajes />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
