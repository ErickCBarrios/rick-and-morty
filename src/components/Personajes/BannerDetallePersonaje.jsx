export function BannerDetallePersonaje({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  function getColorStatus(status) {
    let color = 'green';

    if (status === 'unknown') {
      color = 'orange';
    }

    if (status === 'Dead') {
      color = 'red';
    }
    const circle = {
      width: '10px',
      height: '10px',
      display: 'inline-block',
      backgroundColor: color,
      borderRadius: '50%',
      marginRight: '5px',
    };
    return circle;
  }

  return (
    <div className='col-6'>
      <div className='card bg-dark mb-3'>
        <div className='row g-0'>
          <div className='col-md-4' style={{ height: '300px' }}>
            <img
              style={{ height: '100%', objectFit: 'cover' }}
              src={image}
              className='img-fluid rounded-start'
              alt={name}
            />
          </div>
          <div className='col-md-8'>
            <div className='card-body'>
              <h5 className='card-title mb-0 text-white'>{name}</h5>

              <p style={{ color: 'white' }}>
                <span style={getColorStatus(status)}></span>
                {status} - {species}
              </p>

              <p className='mb-0 text-muted'>last know location</p>
              <p style={{ color: 'white' }}>{location?.name}</p>

              <p className='mb-0 text-muted'>Origin</p>
              <p style={{ color: 'white' }}>{origin?.name}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
