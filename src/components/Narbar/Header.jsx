import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav className='navbar fixed-top navbar-expand-lg navbar-dark bg-dark'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/'>
          Rick and morty
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li>
              <Link className='navbar-brand' to='/'>
                Home
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
