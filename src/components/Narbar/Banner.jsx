import bannerImg from '../../assets/img/rick-and-morty.jpg';
import bannerImg2 from '../../assets/img/rick2.jpeg';
import bannerImg3 from '../../assets/img/rick3.jpg';

export function Banner() {
  return (
    <div
      id='carouselExampleFade'
      className='carousel slide carousel-fade'
      data-bs-ride='carousel'>
      <div className='carousel-inner'>
        <div className='carousel-item active'>
          <img src={bannerImg} className='d-block w-100' alt='...' />
        </div>
        <div className='carousel-item'>
          <img src={bannerImg2} className='d-block w-100' alt='...' />
        </div>
        <div className='carousel-item'>
          <img src={bannerImg3} className='d-block w-100' alt='...' />
        </div>
      </div>
      <button
        className='carousel-control-prev'
        type='button'
        data-bs-target='#carouselExampleFade'
        data-bs-slide='prev'>
        <span className='carousel-control-prev-icon' aria-hidden='true'></span>
        <span className='visually-hidden'>Previous</span>
      </button>
      <button
        className='carousel-control-next'
        type='button'
        data-bs-target='#carouselExampleFade'
        data-bs-slide='next'>
        <span className='carousel-control-next-icon' aria-hidden='true'></span>
        <span className='visually-hidden'>Next</span>
      </button>
    </div>
  );
}
