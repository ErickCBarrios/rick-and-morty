export function Footer() {
  return (
    <div className='navbar-dark fixed-bottom bg-dark'>
      <p className='text-center text-white py-4 mb-0'>
        Proyecto creado por Erick Contreras 2022
      </p>
    </div>
  );
}
