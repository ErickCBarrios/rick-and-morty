export function Buscador({ valor, onBuscar }) {
  return (
    <div className='d-flex justify-content-end' style={{ margin: '25px' }}>
      <div className='mb-3 col-5'>
        <label for='exampleFormControlInput1' className='form-label'>
          <p style={{ color: 'white' }}>Buscar</p>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Buscar Personajes...'
          value={valor}
          onChange={(evento) => {
            onBuscar(evento.target.value);
          }}
        />
      </div>
    </div>
  );
}
