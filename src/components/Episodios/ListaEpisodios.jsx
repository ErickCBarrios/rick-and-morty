import { Episodios } from './Episodios';

export function ListaEpisodios({ episodios }) {
  return (
    <div className='row py-5 mr-auto'>
      {episodios.map((episodio) => {
        return <Episodios key={episodio.data.id} {...episodio.data} />;
      })}
    </div>
  );
}
