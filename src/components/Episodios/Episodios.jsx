export function Episodios({ name, air_date, episode }) {
  return (
    <div className='col-sm-6 col-lg-4'>
      <div
        className='card text-white bg-dark mb-3'
        style={{ maxWidth: '18rem' }}>
        <div className='card-header'>{name}</div>
        <div className='card-body'>
          <h5 className='card-title'>{air_date}</h5>
          <p className='card-text'>{episode}</p>
        </div>
      </div>
    </div>
  );
}
