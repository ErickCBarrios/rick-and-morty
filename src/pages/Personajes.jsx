import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { BannerDetallePersonaje } from '../components/Personajes/BannerDetallePersonaje';
import { ListaEpisodios } from '../components/Episodios/ListaEpisodios';
export function Personajes() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });

      Promise.all(peticionesEpisodios).then((respuesta) => {
        setEpisodios(respuesta);
      });
    }
  }, [personaje]);

  return (
    <div className='p-5'>
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />
          <h2 className='py-5 text-white text-center'>Episodios</h2>

          {episodios ? (
            <ListaEpisodios episodios={episodios} />
          ) : (
            <div>Cargando...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}
