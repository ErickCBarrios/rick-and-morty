import { Banner } from '../components/Narbar/Banner';
import { Buscador } from '../components/Buscador/Buscador';
import { ListadoPersonajes } from '../components/Personajes/ListadoPersonajes';
import { useState } from 'react';
export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-4 text-white'>Personajes Destacados</h1>
        <Buscador valor={buscador} onBuscar={setBuscador} />
        <ListadoPersonajes buscar={buscador} />
      </div>
    </div>
  );
}
